create database student2;
create database  teacher;
drop database student;
show databases;
use student2;
create table student2(StudentId int primary key,FirstName varchar(25),LastName varchar(25),Age int,Location varchar(25) );
create table Teacher(TeacherId int not null,Subject varchar(25),StudentId int ,
FOREIGN KEY (StudentId) REFERENCES student2(StudentId) );


show tables;


insert into Student2 values(1,"Paluru","Pravallika",23,"mydukur");
insert into Student2 values(2,"Perla","Pavani",24,"Kadapa");
insert into Student2 values(3,"Gangi","	Sravani",27,"Nellore");
insert into Student2 values(4,"Kota","Gaya",22,"Kurnool");
insert into Student2 values(5,"Tallam","Yaswi",25,"Hyderabad");
select  * from Student2;

insert into Teacher values(1, "Maths", 1);
insert into Teacher values(2, "	Science", 3);
insert into Teacher values(3, "Telugu", 2);
insert into Teacher values(4, "English", 5);
insert into Teacher values(5, "Social", 3);

select * from Teacher;

select StudentId, FirstName, Age from Student2 where Age>16;
select StudentId, FirstName, Age from Student2 where StudentId>1;

Select  StudentId, Age, FirstName from  Student2 Group By  FirstName ;
select TeacherId, Subject from Teacher group by Subject;
Select  StudentId, Age, FirstName from  Student2 Group By  FirstName order by FirstName asc ;

alter table Student2 add marks int ;
alter table Student2 rename to Student;
select * from Student;
update Student2
set marks=80 where StudentId =1;

update Student2 set marks=70 where StudentId =2;
update Student2 set marks=90 where StudentId =3;
update Student2 set marks=60 where  StudentId =5;
update Student2 set marks=50 where  Studentid =4;


select FirstName ,sum(marks) from Student2;
select age, avg(marks) from Student2;
select FirstName, age ,min(marks) from Student2  group by StudentId order by StudentId desc;

     
SELECT StudentId, FirstName, LastName, Age 
FROM Student2
WHERE StudentId IN (1, 3, 5);
SELECT StudentId, FirstName, LastName, Age 
FROM Student2
WHERE StudentId between  1 and  4;
select  FirstName ,LastName, TeacherId from Student2 inner join Teacher on Student2.StudentId= Teacher.StudentId;
Select  FirstName ,LastName, TeacherId from Student2 left join Teacher on  Student2.StudentId= Teacher.StudentId;
Select  LastName, TeacherId, Subject from Student2 right join Teacher on  Student2.StudentId= Teacher.StudentId;
Select  *from Student2 full  join Teacher on  Student2.StudentId= Teacher.StudentId;
Select  FirstName ,LastName, TeacherId from Student2 cross join Teacher on  Student2.StudentId= Teacher.StudentId;

