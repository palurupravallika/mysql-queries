show tables;
show databases;

use employee;


CREATE database Employee;



create table employee(EmpId INTEGER PRIMARY KEY, 	FirstName VARCHAR(20),LastName VARCHAR(20), Email VARCHAR(25), Salary INTEGER);
 
 
ALTER TABLE employee
DROP COLUMN phone_number;
DELETE FROM employee WHERE EmpId = 1;
INSERT INTO employee values (1,'paluru','pravallika','ppravallika@gmail.com',5000);
INSERT INTO employee values (2,'chitirala','likith','likith12@gmail.com',4000);
INSERT INTO employee values (3,'tallam','yaswi','tallamyaswi@gmail.com',3000);
INSERT INTO employee values (4,'sai','teja','saitejas@gmail.com',2000);
INSERT INTO employee values (5,'perla','pavani','perlapavani@gmail.com',6000);

select * from employee;




ALTER TABLE employee ADD column phone_number bigint;
 
update employee set phone_number ='8341099043' where EmpId=3;
update employee set phone_number ='9501099043' where EmpId=4;
update employee set phone_number ='9014457595' where EmpId=5;
 delete from employee where EmpId = NULL;
 
 select sum(Salary) from employee;
 
 select * from employee where salary >2000;
 select * from employee where salary <2000;
 
 select Empid ,count(salary) from employee group by EmpId having count(salary)>2000;
 select count(EmpId),Email from employee group by Email having count(EmpId) >1;
 
 select * from employee 
 order by FirstName asc;

 select EmpId, sum(Salary) from employee group by EmpId;
  select EmpId, sum(Salary) from employee group by EmpId order by sum(salary)asc;
  
  select Avg(Salary) from Employee;
  select EmpId,FirstName,LastName,Avg(Salary) from Employee group by EmpId;
    select EmpId,FirstName,LastName,Avg(Salary) from Employee group by EmpId order by Avg(Salary)desc;
    
    SELECT COUNT(EmpId)  FROM employee;
    select EmpId, count(Salary) as count from employee group by EmpId;
    
    select max(salary) from employee;
    SELECT  EmpId, max(salary)  from employee group by EmpId order by max(salary)desc;
    
    select min(salary)from employee;
    select  EmpId,FirstName, LastName, min(salary)from employee group by EmpId ;
    select  EmpId,FirstName, LastName, min(salary)from employee group by EmpId order by min(salary)asc;
    
    select EmpId,FirstName,LastName,Salary from employee  where Salary between  3000 and 6000;
     select EmpId,FirstName,LastName,Salary from employee  where Salary  not between  2000 and 6000;
     
SELECT EmpId, FirstName, LastName, Salary
FROM Employee
WHERE EmpId IN (1, 3, 5);

select  EmpId, FirstName, LastName, Salary
FROM Employee
WHERE EmpId NOT IN (1, 3, 5);

SELECT *
FROM employee
WHERE FirstName LIKE 'P%';

select distinct FirstName from employee;

    