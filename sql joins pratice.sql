mysql: [Warning] C:\Program Files\MySQL\MySQL Server 8.0\bin\mysql.exe: ignoring option '--no-beep' due to invalid value ''.
Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 17
Server version: 8.0.22 MySQL Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.



mysql>  show databases;
+--------------------+
| Database           |
+--------------------+
| assessment1        |
| book               |
| departments        |
| employee           |
| information_schema |
| jpadb              |
| mysql              |
| performance_schema |
| persons            |
| petpeerproject     |
| petproject         |
| petuserproject     |
| sakila             |
| springjpadb        |
| sys                |
| world              |
+--------------------+
16 rows in set (0.01 sec)



mysql> create database customers;
Query OK, 1 row affected (0.02 sec)

mysql> create database products;
Query OK, 1 row affected (0.01 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| assessment1        |
| book               |
| customers          |
| employee           |
| information_schema |
| jpadb              |
| mysql              |
| performance_schema |
| persons            |
| petpeerproject     |
| petproject         |
| petuserproject     |
| products           |
| sakila             |
| springjpadb        |
| sys                |
| world              |
+--------------------+
17 rows in set (0.01 sec)

mysql> use customers;
Database changed
mysql> create table customers(id INTEGER PRIMARY KEY,
    ->     name VARCHAR(100),
    ->     age INTEGER
    ->   );
Query OK, 0 rows affected (0.08 sec)

mysql> alter table customers add country varchar
    -> (25);
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from customers;
Empty set (0.00 sec)

mysql> desc customers;
+---------+--------------+------+-----+---------+-------+
| Field   | Type         | Null | Key | Default | Extra |
+---------+--------------+------+-----+---------+-------+
| id      | int          | NO   | PRI | NULL    |       |
| name    | varchar(100) | YES  |     | NULL    |       |
| age     | int          | YES  |     | NULL    |       |
| country | varchar(25)  | YES  |     | NULL    |       |
+---------+--------------+------+-----+---------+-------+
4 rows in set (0.01 sec)

mysql> create database orders;
Query OK, 1 row affected (0.02 sec)


mysql> create table orders(order_id INT,
    ->   product VARCHAR(40),
    ->   total INT,
    ->   customer_id INT,
    ->   CONSTRAINT OrdersPK PRIMARY KEY (order_id),
    ->   FOREIGN KEY (customer_id) REFERENCES Customers(id)
    -> );
Query OK, 0 rows affected (0.11 sec)

mysql> alter table customers rename column name to first_name;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table customers add column last_name varchar(25);
Query OK, 0 rows affected (0.03 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc customers;
+------------+--------------+------+-----+---------+-------+
| Field      | Type         | Null | Key | Default | Extra |
+------------+--------------+------+-----+---------+-------+
| id         | int          | NO   | PRI | NULL    |       |
| first_name | varchar(100) | YES  |     | NULL    |       |
| age        | int          | YES  |     | NULL    |       |
| country    | varchar(25)  | YES  |     | NULL    |       |
| last_name  | varchar(25)  | YES  |     | NULL    |       |
+------------+--------------+------+-----+---------+-------+
5 rows in set (0.01 sec)



mysql> desc customers;
+---------+--------------+------+-----+---------+-------+
| Field   | Type         | Null | Key | Default | Extra |
+---------+--------------+------+-----+---------+-------+
| id      | int          | NO   | PRI | NULL    |       |
| name    | varchar(100) | YES  |     | NULL    |       |
| age     | int          | YES  |     | NULL    |       |
| country | varchar(25)  | YES  |     | NULL    |       |
+---------+--------------+------+-----+---------+-------+
4 rows in set (0.01 sec)

mysql> insert into customers values(1,'raviteja',25,'bangalore');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customers values(2,'likith',27,'chennai');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customers values(3,'siva',23,'hyderabad');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customers values(4,'sakshi',26,'kurnool');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customers values(5,'pradeep',22,'tirupathi');
Query OK, 1 row affected (0.01 sec)

mysql> select * from customers;
+----+----------+------+-----------+
| id | name     | age  | country   |
+----+----------+------+-----------+
|  1 | raviteja |   25 | bangalore |
|  2 | likith   |   27 | chennai   |
|  3 | siva     |   23 | hyderabad |
|  4 | sakshi   |   26 | kurnool   |
|  5 | pradeep  |   22 | tirupathi |
+----+----------+------+-----------+
5 rows in set (0.01 sec)


mysql> INSERT INTO Orders
    -> VALUES
    -> (1, 'Keyboard', 400, 2),
    -> (2, 'Mouse', 300, 2),
    -> (3, 'Monitor', 12000, 1),
    -> (4,'computer',7000,3),
    -> (5,'printer',5000,4);
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from orders;
+----------+----------+-------+-------------+
| order_id | product  | total | customer_id |
+----------+----------+-------+-------------+
|        1 | Keyboard |   400 |           2 |
|        2 | Mouse    |   300 |           2 |
|        3 | Monitor  | 12000 |           1 |
|        4 | computer |  7000 |           3 |
|        5 | printer  |  5000 |           4 |
+----------+----------+-------+-------------+
5 rows in set (0.00 sec)

mysql> desc customers;


mysql> alter table customers rename column id to customer_id;
Query OK, 0 rows affected (0.03 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table customers rename column name to customerName;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0




mysql> select customers.customer_id,customerName,orders.order_id,product
    -> from customers
    -> inner join orders
    -> on customers.customer_id=orders.customer_id;
+-------------+--------------+----------+----------+
| customer_id | customerName | order_id | product  |
+-------------+--------------+----------+----------+
|           2 | likith       |        1 | Keyboard |
|           2 | likith       |        2 | Mouse    |
|           1 | raviteja     |        3 | Monitor  |
|           3 | siva         |        4 | computer |
|           4 | sakshi       |        5 | printer  |
+-------------+--------------+----------+----------+
5 rows in set (0.01 sec)



mysql> select customers.customer_id,customerName,country,orders.order_id,product
    -> from customers
    -> left join orders
    -> on customers.customer_id =orders.customer_id;
+-------------+--------------+-----------+----------+----------+
| customer_id | customerName | country   | order_id | product  |
+-------------+--------------+-----------+----------+----------+
|           1 | raviteja     | bangalore |        3 | Monitor  |
|           2 | likith       | chennai   |        1 | Keyboard |
|           2 | likith       | chennai   |        2 | Mouse    |
|           3 | siva         | hyderabad |        4 | computer |
|           4 | sakshi       | kurnool   |        5 | printer  |
|           5 | pradeep      | tirupathi |     NULL | NULL     |
+-------------+--------------+-----------+----------+----------+
6 rows in set (0.00 sec)

mysql> select * from customers
    -> left join orders
    -> on customers.customer_id=orders.customer_id;
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
| customer_id | customerName | age  | country   | order_id | product  | total | customer_id |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
|           1 | raviteja     |   25 | bangalore |        3 | Monitor  | 12000 |           1 |
|           2 | likith       |   27 | chennai   |        1 | Keyboard |   400 |           2 |
|           2 | likith       |   27 | chennai   |        2 | Mouse    |   300 |           2 |
|           3 | siva         |   23 | hyderabad |        4 | computer |  7000 |           3 |
|           4 | sakshi       |   26 | kurnool   |        5 | printer  |  5000 |           4 |
|           5 | pradeep      |   22 | tirupathi |     NULL | NULL     |  NULL |        NULL |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
6 rows in set (0.00 sec)

mysql> select customers.customer_id,customerName,country,orders.order_id,product
    -> from customers
    -> right join orders
    -> on customers.customer_id=orders.customer_id;
+-------------+--------------+-----------+----------+----------+
| customer_id | customerName | country   | order_id | product  |
+-------------+--------------+-----------+----------+----------+
|           2 | likith       | chennai   |        1 | Keyboard |
|           2 | likith       | chennai   |        2 | Mouse    |
|           1 | raviteja     | bangalore |        3 | Monitor  |
|           3 | siva         | hyderabad |        4 | computer |
|           4 | sakshi       | kurnool   |        5 | printer  |
+-------------+--------------+-----------+----------+----------+
5 rows in set (0.00 sec)

mysql> select * from customers
    -> cross join orders
    -> on customers.customer_id=orders.customer_id;
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
| customer_id | customerName | age  | country   | order_id | product  | total | customer_id |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
|           1 | raviteja     |   25 | bangalore |        3 | Monitor  | 12000 |           1 |
|           2 | likith       |   27 | chennai   |        1 | Keyboard |   400 |           2 |
|           2 | likith       |   27 | chennai   |        2 | Mouse    |   300 |           2 |
|           3 | siva         |   23 | hyderabad |        4 | computer |  7000 |           3 |
|           4 | sakshi       |   26 | kurnool   |        5 | printer  |  5000 |           4 |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
5 rows in set (0.00 sec)

mysql> select * from customers
    -> cross join orders;
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
| customer_id | customerName | age  | country   | order_id | product  | total | customer_id |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
|           1 | raviteja     |   25 | bangalore |        1 | Keyboard |   400 |           2 |
|           2 | likith       |   27 | chennai   |        1 | Keyboard |   400 |           2 |
|           3 | siva         |   23 | hyderabad |        1 | Keyboard |   400 |           2 |
|           4 | sakshi       |   26 | kurnool   |        1 | Keyboard |   400 |           2 |
|           5 | pradeep      |   22 | tirupathi |        1 | Keyboard |   400 |           2 |
|           1 | raviteja     |   25 | bangalore |        2 | Mouse    |   300 |           2 |
|           2 | likith       |   27 | chennai   |        2 | Mouse    |   300 |           2 |
|           3 | siva         |   23 | hyderabad |        2 | Mouse    |   300 |           2 |
|           4 | sakshi       |   26 | kurnool   |        2 | Mouse    |   300 |           2 |
|           5 | pradeep      |   22 | tirupathi |        2 | Mouse    |   300 |           2 |
|           1 | raviteja     |   25 | bangalore |        3 | Monitor  | 12000 |           1 |
|           2 | likith       |   27 | chennai   |        3 | Monitor  | 12000 |           1 |
|           3 | siva         |   23 | hyderabad |        3 | Monitor  | 12000 |           1 |
|           4 | sakshi       |   26 | kurnool   |        3 | Monitor  | 12000 |           1 |
|           5 | pradeep      |   22 | tirupathi |        3 | Monitor  | 12000 |           1 |
|           1 | raviteja     |   25 | bangalore |        4 | computer |  7000 |           3 |
|           2 | likith       |   27 | chennai   |        4 | computer |  7000 |           3 |
|           3 | siva         |   23 | hyderabad |        4 | computer |  7000 |           3 |
|           4 | sakshi       |   26 | kurnool   |        4 | computer |  7000 |           3 |
|           5 | pradeep      |   22 | tirupathi |        4 | computer |  7000 |           3 |
|           1 | raviteja     |   25 | bangalore |        5 | printer  |  5000 |           4 |
|           2 | likith       |   27 | chennai   |        5 | printer  |  5000 |           4 |
|           3 | siva         |   23 | hyderabad |        5 | printer  |  5000 |           4 |
|           4 | sakshi       |   26 | kurnool   |        5 | printer  |  5000 |           4 |
|           5 | pradeep      |   22 | tirupathi |        5 | printer  |  5000 |           4 |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
25 rows in set (0.00 sec)



mysql> select * from customers
    -> full join orders;
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
| customer_id | customerName | age  | country   | order_id | product  | total | customer_id |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
|           1 | raviteja     |   25 | bangalore |        1 | Keyboard |   400 |           2 |
|           2 | likith       |   27 | chennai   |        1 | Keyboard |   400 |           2 |
|           3 | siva         |   23 | hyderabad |        1 | Keyboard |   400 |           2 |
|           4 | sakshi       |   26 | kurnool   |        1 | Keyboard |   400 |           2 |
|           5 | pradeep      |   22 | tirupathi |        1 | Keyboard |   400 |           2 |
|           1 | raviteja     |   25 | bangalore |        2 | Mouse    |   300 |           2 |
|           2 | likith       |   27 | chennai   |        2 | Mouse    |   300 |           2 |
|           3 | siva         |   23 | hyderabad |        2 | Mouse    |   300 |           2 |
|           4 | sakshi       |   26 | kurnool   |        2 | Mouse    |   300 |           2 |
|           5 | pradeep      |   22 | tirupathi |        2 | Mouse    |   300 |           2 |
|           1 | raviteja     |   25 | bangalore |        3 | Monitor  | 12000 |           1 |
|           2 | likith       |   27 | chennai   |        3 | Monitor  | 12000 |           1 |
|           3 | siva         |   23 | hyderabad |        3 | Monitor  | 12000 |           1 |
|           4 | sakshi       |   26 | kurnool   |        3 | Monitor  | 12000 |           1 |
|           5 | pradeep      |   22 | tirupathi |        3 | Monitor  | 12000 |           1 |
|           1 | raviteja     |   25 | bangalore |        4 | computer |  7000 |           3 |
|           2 | likith       |   27 | chennai   |        4 | computer |  7000 |           3 |
|           3 | siva         |   23 | hyderabad |        4 | computer |  7000 |           3 |
|           4 | sakshi       |   26 | kurnool   |        4 | computer |  7000 |           3 |
|           5 | pradeep      |   22 | tirupathi |        4 | computer |  7000 |           3 |
|           1 | raviteja     |   25 | bangalore |        5 | printer  |  5000 |           4 |
|           2 | likith       |   27 | chennai   |        5 | printer  |  5000 |           4 |
|           3 | siva         |   23 | hyderabad |        5 | printer  |  5000 |           4 |
|           4 | sakshi       |   26 | kurnool   |        5 | printer  |  5000 |           4 |
|           5 | pradeep      |   22 | tirupathi |        5 | printer  |  5000 |           4 |
+-------------+--------------+------+-----------+----------+----------+-------+-------------+
25 rows in set (0.00 sec)


mysql> alter table orders rename column total to amount;
Query OK, 0 rows affected (0.02 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from orders;
+----------+----------+--------+-------------+
| order_id | product  | amount | customer_id |
+----------+----------+--------+-------------+
|        1 | Keyboard |    400 |           2 |
|        2 | Mouse    |    300 |           2 |
|        3 | Monitor  |  12000 |           1 |
|        4 | computer |   7000 |           3 |
|        5 | printer  |   5000 |           4 |
+----------+----------+--------+-------------+
5 rows in set (0.00 sec)

mysql> select customers.customer_id,customerName,country,orders.order_id,product
    -> from customers
    -> left join orders
    -> on customers.customer_id=orders. customer_id
    -> WHERE Orders.amount >= 500;
+-------------+--------------+-----------+----------+----------+
| customer_id | customerName | country   | order_id | product  |
+-------------+--------------+-----------+----------+----------+
|           1 | raviteja     | bangalore |        3 | Monitor  |
|           3 | siva         | hyderabad |        4 | computer |
|           4 | sakshi       | kurnool   |        5 | printer  |
+-------------+--------------+-----------+----------+----------+
3 rows in set (0.00 sec)

mysql> select customers.customer_id,customerName,country,orders.order_id,product
    -> from customers
    -> right join orders
    -> on customers.customer_id=orders. customer_id
    -> WHERE Orders.amount <= 500;
+-------------+--------------+---------+----------+----------+
| customer_id | customerName | country | order_id | product  |
+-------------+--------------+---------+----------+----------+
|           2 | likith       | chennai |        1 | Keyboard |
|           2 | likith       | chennai |        2 | Mouse    |
+-------------+--------------+---------+----------+----------+
2 rows in set (0.00 sec)

mysql>