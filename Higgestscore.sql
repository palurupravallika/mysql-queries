show databases;
use gradesystem;
show tables;
SELECT * FROM gradesystem.student_details;
SELECT * FROM gradesystem.subjects;
SELECT * FROM gradesystem.group;
SELECT * FROM gradesystem.marks;
 
SET FOREIGN_KEY_CHECKS=0;

select marks. MarkID,marks.StudentRollNo,marks.SubjectCode ,student_details.StudentName  from marks   join Student_details   on  marks.StudentRollNO= student_details.StudentRollNo;

select StudentRollNo,SubjectCode, Subject1,GroupID, MAX(Subject1) AS highestscore
FROM marks 
INNER JOIN subjects 
 ON marks.SubjectCode = subjects.SubjectCode
INNER JOIN  group
 ON subjects.GroupID = group.GroupID
GROUP BY StudentRollNo, SubjectCode, Subject1,GroupID;

show databases;